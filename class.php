<?php

class Bot
{
	private $source;
	public $information;

	public function __construct($link)
	{
		$this->link = $link;
		$this->connect();
	}

	public function connect()
	{
		try
		{
			$this->information = array();
			$this->site_connect();
			$this->parse_title();
			$this->parse_content();
		}
		catch(Exception $e)
		{
			exit($e->getMessage());
		}
	}

	private function site_connect()
	{
		$link = $this->link;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/52.0.2743.116 Chrome/52.0.2743.116 Safari/537.36");
		curl_setopt($ch, CURLOPT_REFERER, $link);
		curl_setopt($ch, CURLOPT_URL, $link);
		$str = curl_exec($ch);
		$curl_errno = curl_errno($ch);
		curl_close($ch);
		if($curl_errno) {
			throw new Exception("Hata: Bağlantı Hatası");
		}
		$this->source = $str;
	}

	private function parse_title()
	{
		$p = '~<span itemprop="name">(.*?)</span>~s';
		if(preg_match($p, $this->source, $match))
		{
			$this->information['title'] = $this->clean_data($match[1]);
		}
		else
		{
			throw new Exception("Hata: Film Adı Bulunamadı!");
		}
	}

	private function parse_content()
	{
		$p = '~<div class="ozet-goster" itemprop="description">(.*?)</div>~s';
		if(preg_match($p, $this->source, $match))
		{
			$this->information['content'] = $this->clean_data(explode('|', $match[1])[0]);
		}
		else
		{
			throw new Exception("Hata: Film İçeriği Bulunamadı!");			
		}
	}

	private function clean_data($str)
	{
		$str = trim($str);
		$str = preg_replace('~\s{1,}~', ' ', $str);
		$str = preg_replace('~<br\s?\/?>~i', "\n", $str);  
		return $str;
	}
}
